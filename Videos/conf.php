<?php

//Fichier contenant la liste des sondages
$liste="listesondages.txt";

//Fichier de log pour savoir quel questionnaire a été choisi
$log="logtapas.txt";

$contenu=file($liste);
$faits=file($log);

//Nombre de sondages disponibles
$nb = count($contenu);

// Un sondage au hasard
$tirage = rand(0,$nb-1);

$numsondage = $contenu[$tirage];
$i=0;
foreach ($faits as $nombre) {
	if ($numsondage == $nombre) {
		$i=$i+1;
	} ;
};

$url = "https://sondages.lisn.upsaclay.fr/tapas/index.php";
$sondage = $url."/".$contenu[$tirage];


// On ajoute dans le fichier de log
$fp = fopen($log, 'a');
fwrite($fp, $numsondage);

// A vérifier
if ($i > 4) {
fwrite($fp, "ALERTE [$i] : $numsondage");
};

fclose($fp);
?>
