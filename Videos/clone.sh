#!/bin/bash

# 1 - Exporter un questionnaire qui servira de modèle (patron.lss dans l'exemple ci-dessous). 

modele="patron.lss"

# 2 - Nom du fichier vidéo à remplacer (on peut editer patron.lss si la chaîne est trop grande)

chaine="AREMPLACER.mp4"

# 3 - Indiquer le nom du fichier contenant la liste des vidéos. 

liste="listevideos.txt"

# 4 - Lancer le script par la commande ./clone.sh


litlaliste() {
 while read video $fichier; do
	 i=$(($i+1))
	 #Nom du fichier contenant le nouveau questionnaire
	 #Dans un dossier "out" pour pouvoir effacer / créer des zip
	 #q1, q2, q3... On peut changer
	 nouveau="out/q$i.lss"

	 cp $modele $nouveau
	 sed -i -e "s/$chaine/$video/g" $nouveau
 done
}

i=0
litlaliste < $liste
