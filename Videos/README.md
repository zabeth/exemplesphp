# Vidéo au hasard pour un questionnaire Limesurvey. 

[Le fichier index.php](index.php) permet d'afficher un questionnaire Limesurvey au hasard. 

[randomvideo.php](randomvideo.php) permet d'afficher une vidéo au hasard. Ce fichier peut être inclu dans un questionnaire Limesurvey. 

[clone.sh](clone.sh) permet de dupliquer un questionnaire Limesurvey [exporté](patron.lss) en remplaçant le nom du fichier vidéo par ceux d'une liste.

![exemple d'export](exportlss.png)
